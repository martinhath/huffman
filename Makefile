CC = clang
FLAGS = -g -Wall -Wextra
HUFF = huffman.c
TEST = test_heap.c test_huffman.c

all: compress decompress


compress:
	$(CC) $(FLAGS) $(HUFF) compress.c -o compress

decompress:
	$(CC) $(FLAGS) $(HUFF) decompress.c -o decompress

test: compress decompress
	$(CC) $(FLAGS) $(HUFF) $(TEST) test.c -o test
	./test


clean:
	rm **/*.o

.PHONY: compress decompress test
