#include "huffman.h"

#include <stdio.h>

static int test_swap(void) {
    Node n1 = {3, 'e', NULL, NULL};
    Node n2 = {1, 'l', NULL, NULL};

    printf("w: %zu\tc: %c\n", n1.weight, n1.letter);
    printf("w: %zu\tc: %c\n", n2.weight, n2.letter);
    node_swap(&n1, &n2);
    printf("-- swap --\n");
    printf("w: %zu\tc: %c\n", n1.weight, n1.letter);
    printf("w: %zu\tc: %c\n", n2.weight, n2.letter);
    return 0;
}

static int test_push_pop(void) {
    Heap *heap = heap_create(0);
    Node n1 = {1, 'a', NULL, NULL};
    Node n2 = {99123, 'n', NULL, NULL};
    Node n3 = {4096, 't', NULL, NULL};
    Node n4 = {65536, 'i', NULL, NULL};
    Node n5 = {0, 'm', NULL, NULL};
    Node n6 = {1337, 'r', NULL, NULL};

    heap_insert(heap, &n1);
    heap_insert(heap, &n2);
    heap_insert(heap, &n3);
    heap_insert(heap, &n4);
    heap_insert(heap, &n5);
    heap_insert(heap, &n6);

    int i = 6;
    while (i --> 0)
        printf("%c", heap_pop(heap)->letter);
    printf("\n%d\n", heap_is_empty(heap));
    return 0;
}

int test_heap(void) {
    test_swap();
    test_push_pop();

    return 0;
}
