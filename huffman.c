#include "huffman.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


static Node treeroot;

static char** create_lookup(char**, Node*, char*);

int huffman_init()
{
    return 0;
}

int huffman_add_byte(byte b, size_t f)
{
    int overwrite = !table[b];
    table[b] = f;
    if (overwrite)
        return 1;
    return 0;
}

static char* lookup[256];

int huffman_create_tree()
{
    Heap *heap = heap_create(0);
    for (size_t i = 0; i < 256; i++){
        byte b = table[i];
        if (b == 0) continue;

        Node *n = malloc(sizeof(Node));
        n->weight = b;
        n->letter = i;

        heap_insert(heap, n);
    }

    while (heap_size(heap) > 1) {
        Node *n1 = heap_pop(heap);
        Node *n2 = heap_pop(heap);
        Node n3 = node_join_nodes(n1, n2);

        Node *n = malloc(sizeof(Node));
        node_swap(&n3, n);

        heap_insert(heap, n);
    }
    Node *tree = heap_pop(heap);
    // Need to assign codes to the characters.
    // Also need to find out how we want to 
    // represent the codes (bitstrings?).
    treeroot = *tree;

    char* table[256] = {};

    // top hack; in order to not segfault when
    // free() is called on the third argument 
    // in `create_lookup`.
    char* s = malloc(1);
    s[0] = '\0';

    create_lookup(table, tree, s);

    for (int i = 0; i < 256; i++){
        if (table[i] != NULL){
            lookup[i] = table[i];
        }
    }

    return 0;
}

static char** create_lookup(char** lookup, Node* tree, char* prefix) {
    if (node_is_leaf(tree)) {
        lookup[tree->letter] = prefix;
        return lookup;
    }
    size_t pre_len = strlen(prefix);

    if (tree->left != NULL){
        char* left_str = malloc(pre_len + 2);
        memmove(left_str, prefix, pre_len);
        strncat(left_str, "0", 1);
        create_lookup(lookup, tree->left, left_str);
    }
    if (tree->right != NULL){
        char *right_str = malloc(pre_len + 2);
        memmove(right_str, prefix, pre_len+2);
        strncat(right_str, "1", 1);
        create_lookup(lookup, tree->right, right_str);
    }
    free(prefix);
    return lookup;
}

int huffman_encrypt(byte* string, size_t len) {
#define buffer_size 32
    char bytestring[buffer_size];
    char* pstring = bytestring;

    printf("%s\n", lookup['a']);

    size_t i = len;
    while(i --> 0) {
        char c = *string++;
        char *code = lookup[c];
        size_t code_len = strlen(code);
        if (pstring + code_len > bytestring+buffer_size) {
            // The buffer is full; print out what
            // we've got.
            char *printer = bytestring;
            while (printer+8 <= pstring){
                printf("%d ", str_to_byte(printer));
                printer += 8;
            }
            pstring = bytestring;
        }
        memmove(pstring, code, code_len);
        pstring += code_len;
    }
    // Adds padding
    size_t off = pstring - bytestring;
    off = (off/8) * 8;
    while (bytestring + off > pstring){
        *pstring++ = '\0';
    }

    char *printer = bytestring;
    while (printer < pstring){
        printf("%d ", str_to_byte(printer));
        printer += 8;
    }
    return 0;
}

byte BITS[] = {1, 2, 4, 8, 16, 32, 64, 128};
byte str_to_byte(char *string){
    byte b = 0;
    size_t i = 8;
    while (i-->0){
        b += string[i] == '1' ? BITS[i] : 0;
    }
    return b;
}

Node node_join_nodes(Node *left, Node *right)
{
    Node node;
    node.letter = 0;
    node.weight = left->weight + right->weight;
    node.left = left;
    node.right = right;
    return node;
}

int node_is_leaf(Node *node)
{
    return node->left == NULL &&
           node->right == NULL;
}


int node_compare(Node *l, Node *r)
{
    return l->weight > r->weight;
}


//// Heap
#define DEFAULT_CAPACITY 64
#define DEFAULT_RESIZE ((heap->capacity*3)/2)

Heap *heap_create(size_t initial_cap)
{
    Heap *heap = malloc(sizeof(Heap));
    
    if (initial_cap == 0)
        initial_cap = DEFAULT_CAPACITY;
        
    heap->nelems = 0;
    heap->capacity = initial_cap;
    heap->array = calloc(initial_cap, sizeof(Node *));
    
    return heap;
}

int heap_is_empty(Heap *heap)
{
    return heap->nelems == 0;
}

int heap_resize(Heap *heap, size_t new_capacity)
{
    if (new_capacity == 0)
        new_capacity = DEFAULT_RESIZE;
        
    Node **newarr = calloc(new_capacity, sizeof(Node *));
    Node **oldarr = heap->array;
    
    memmove(newarr, oldarr, heap->capacity);
    heap->array = newarr;
    heap->capacity = new_capacity;

    free(oldarr);
    return 0;
}

int heap_left_child(size_t index)
{
    return 2 * index + 1;
}

int heap_right_child(size_t index)
{
    return 2 * index + 2;
}

int node_swap(Node *a, Node *b)
{
    Node temp;
    memmove(&temp, a, sizeof(Node));
    memmove(a, b, sizeof(Node));
    memmove(b, &temp, sizeof(Node));
    return 0;
}

int heap_insert(Heap *heap, Node *node)
{
    if (heap->nelems >= heap->capacity)
        heap_resize(heap, 0);
    int insert = 0;
    Node *current = heap->array[insert];
    
    while (current != NULL) {
        Node *left = heap->array[heap_left_child(insert)];
        Node *right = heap->array[heap_right_child(insert)];

        if (node_compare(current, node) > 0) {
            node_swap(current, node);
        }
        if (left != NULL && right != NULL) {
            if (node_compare(left, right) < 0)
                insert = heap_left_child(insert);
            else
                insert = heap_right_child(insert);
        } else{
            if (!left)
                insert = heap_left_child(insert);
            else
                insert = heap_right_child(insert);
        }
        current = heap->array[insert];
    }

    heap->array[insert] = node;
    heap->nelems++;

    return 0;
}

Node* heap_pop(Heap* heap) {
    Node *node = heap->array[0];
    heap->array[0] = NULL;

    size_t current = 0;

    while (1) {
        size_t lefti = heap_left_child(current);
        size_t righti = heap_right_child(current);

        Node *left = heap->array[lefti];
        Node *right = heap->array[righti];
        if (left == NULL && right == NULL){
            heap->array[current] = NULL;
            break;
        } else if (left == NULL) {
            heap->array[current] = right;
            current = righti;
        } else if (right == NULL){
            heap->array[current] = left;
            current = lefti;
        } else if (node_compare(left, right) > 0){
            heap->array[current] = right;
            heap->array[righti] = NULL;
            current = righti;
        } else{
            heap->array[current] = left;
            heap->array[lefti] = NULL;
            current = lefti;
        }
    }

    heap->nelems--;
    return node;
}

size_t heap_size(Heap* heap){
    return heap->nelems;
}

void heap_print_array(Heap *heap) {
    for(size_t i = 0; i < heap->nelems * 4; i++){
        Node *node = heap->array[i];
        char c = node != NULL ? node->letter : '/';
        printf("%c ", c);
    }
    printf("\n");
}
