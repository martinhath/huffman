#include "huffman.h"

#include <stdio.h>

int test_huffman() {
    huffman_init();

    huffman_add_byte('a', 12);
    huffman_add_byte('e', 20);
    huffman_add_byte('k', 9);
    huffman_add_byte('y', 11);
    huffman_add_byte('i', 9);
    huffman_add_byte('o', 2);

    huffman_create_tree();

    huffman_encrypt("aaeeaeae", 8);

    /*printf("\n\n");*/
    /*printf("%d == 100\n", str_to_byte("00100110"));*/
    /*printf("%d == 0\n", str_to_byte(  "00000000"));*/
    /*printf("%d == 255\n", str_to_byte("11111111"));*/

    return 0;
}
