#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <stdlib.h>

#define byte unsigned char

typedef struct node {
    size_t weight;
    byte letter;
    struct node *left;
    struct node *right;
} Node;

static byte table[256];

int huffman_init();
int huffman_add_byte(byte b, size_t freq);
int huffman_create_tree();

int huffman_encrypt(byte*, size_t);
int huffman_decrypt(byte*, size_t);


typedef struct heap {
    size_t nelems;
    size_t capacity;
    Node **array;
} Heap;

Heap* heap_create(size_t);
int heap_insert(Heap*, Node*);
Node* heap_pop(Heap*);
int heap_is_empty(Heap*);
int heap_resize(Heap*, size_t);
int heap_left_child(size_t);
int heap_right_child(size_t);
size_t heap_size(Heap*);

int node_swap(Node*, Node*);
int node_compare(Node*, Node*);
Node node_join_nodes(Node*, Node*);
int node_is_leaf(Node*);

void heap_print_array(Heap*);

byte str_to_byte(char*);

#endif
